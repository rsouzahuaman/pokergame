module WinningHands where

import PlayingCard
import Data.List ( groupBy, sortBy )
import Data.Ord ( comparing )

{- By grouping numbers into two groups it is posible to validate some easy hands  -}
checkOutHand :: [Card] -> Maybe (String, [[Card]])
checkOutHand hand =
    let biggestGroup          = head (groupHand hand)
        secondBiggestGroup   = groupHand hand !! 1
    in  case length biggestGroup of
            4 -> Just ("Póquer", [biggestGroup]) -- 4 cards of a same number
            3 -> case length secondBiggestGroup of
                    2 -> Just ("Full house", [biggestGroup, secondBiggestGroup]) -- 3 cards of a same number and 2 cards of a same number
                    _ -> Just ("Tercia o Pierna", [biggestGroup]) -- 3 cards of a same number
            2 -> case length secondBiggestGroup of
                    2 -> Just ("Dos pares", [biggestGroup, secondBiggestGroup]) -- 2 groups of numbers 
                    _ -> Just ("Un par", [biggestGroup]) -- 2 cards of a same number.
            _ -> Just ("Nada", [])
            -- _ -> Nothing

{- This function groups by numbers and return 2 groups. Rank => Number -}
groupHand :: [Card] -> [[Card]]
groupHand hand =
    let equal_rank        = (\x y -> rank x == rank y)
        groups            = groupBy equal_rank $ sortBy (comparing rank) hand 
    in  reverse $ sortBy (comparing length) groups


checkOutHand'2 :: [Card] -> IO Int
checkOutHand'2 hand =
    let biggestGroup          = head (groupHand hand)
        secondBiggestGroup   = groupHand hand !! 1
    in  case length biggestGroup of
            4 -> return 5
            3 -> case length secondBiggestGroup of
                    2 -> return 4
                    _ -> return 3
            2 -> case length secondBiggestGroup of
                    2 -> return 2
                    _ -> return 1
            _ -> return 0