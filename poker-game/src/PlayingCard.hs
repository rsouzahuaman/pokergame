module PlayingCard where

import Data.List ( sortBy )
import Data.Ord ( comparing )
import System.Random

{- Number of cards, we stablish what types of data will be -}
data Rank = Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten
          | Jack | Queen | King | Ace
     deriving (Eq, Ord, Enum)

{- I define what it shows when it is call -}
instance Show Rank where
    show Two   = "Dos"
    show Three = "Tres"
    show Four  = "Cuatro"
    show Five  = "Cinco"
    show Six   = "Seis"
    show Seven = "Siete"
    show Eight = "Ocho"
    show Nine  = "Nueve"
    show Ten   = "Diez"
    show Jack  = "J"
    show Queen = "Q"
    show King  = "K"
    show Ace   = "As"
        
{- Type of cards -}
data Suit = Hearts | Diamonds | Clubs | Spades 
     deriving (Eq)

instance Show Suit where
    show Hearts   = "Corazon"
    show Diamonds = "Diamante"
    show Clubs  = "Trevor"
    show Spades  = "Pica"

{- I difined how is a poker card with its type -}
data Card = Card { rank :: Rank, suit :: Suit }
     deriving (Eq)

{- I difine how must be printed a card-}
instance Show Card where
         show card = show (rank card) ++ " De " ++ show (suit card)

{- I define the posible combination of cards -}
allCards :: [Card]
allCards = [ Card rank suit
            | suit <- [Hearts, Diamonds, Clubs, Spades]
            , rank <- [Two .. Ace] ]

{- I suffle the deck by first making a list of duo, then sorting it by its head -}
shuffle :: StdGen -> [a] -> [a]
shuffle g es = map snd $ sortBy (comparing fst) $ zip rs es
               where rs = randoms g :: [Integer]

doShuffle :: IO [Card]
doShuffle = do gen <- getStdGen
               return (shuffle gen allCards)