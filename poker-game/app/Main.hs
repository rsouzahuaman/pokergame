module Main where

import Lib
import System.IO
import PlayingCard
import Text.Read
import WinningHands
import qualified Control.Monad

main :: IO ()
main = do
    startGame

startGame :: IO ()
startGame = do
    {- I create and shuffle the deck -}
    deck <- doShuffle

    userHand <- getHand deck
    newDeck <- discardFromDeck deck 5
    npcHand <- getHand newDeck
    newDeck <- discardFromDeck deck 5

    putStrLn $ "\n\nTus cartas son: " ++ show userHand
    amount <- getValidaInputInt "Cuantas quieres devolver? (0-4): "

    userHand <- discardHandCards userHand amount
    let newUserHand = userHand ++ take amount newDeck
    newDeck <- discardFromDeck newDeck amount

    putStrLn $ "\nTus nuevas cartas son: " ++ show newUserHand
    putStrLn $ "Tienes: " ++ show (checkOutHand newUserHand)

    putStrLn $ "\nLas cartas del rival son: " ++ show npcHand
    putStrLn $ "El rival tiene: " ++ show (checkOutHand npcHand)
    putStrLn "\n"

    whoWin userHand npcHand
    putStrLn "Fin del juego"
    putStrLn "\nQuieres volver a jugar? S(Si) / cualquier otra tecla(no)"
    result <- getLine
    if result == "S" || result == "s" then startGame else return ()
    -- Control.Monad.when (result == "S" || result == "s") startGame

whoWin :: [Card] -> [Card] -> IO Int
whoWin userHand npcHand = do
    user <- checkOutHand'2 userHand
    npc <- checkOutHand'2 npcHand
    putStrLn $ decide user npc
    return 0

decide :: Int -> Int -> String
decide user npc
   | user > npc = "Felicitaciones Ganaste !!!"
   | npc > user = "Perdiste :("
   | otherwise  = "Empate !!"

getHand :: [Card] -> IO [Card]
getHand deck = do
    let newHand = take 5 deck
    return newHand

discardFromDeck :: [Card] -> Int -> IO [Card]
discardFromDeck deck amount = do
    let newDeck = drop amount deck
    return newDeck

discardHandCards :: [Card] -> Int -> IO [Card]
discardHandCards hand amount
    | amount == 0 = return hand
    | otherwise  = do
        if amount `elem` [1..4]
            then do
                putStrLn "\n"
                putStrLn $ "Tus cartas son: " ++ show hand
                position <- getValidaInputIntWithValidationRange ("Escribir el numero de la posicion de la carta a eliminar. [1 - " ++ show(length hand) ++ "]") [1..length hand]
                let newHand = let (h, t) = splitAt (position - 1) hand in h ++ drop 1 t
                discardHandCards newHand (amount - 1)
            else do
                newAmount <- getValidaInputInt "Debes escribir un numero dentro del rango [1..4]"
                discardHandCards hand newAmount


{- Function that recibes a string and validate the input of a getLine -}
getValidaInputInt :: String -> IO Int
getValidaInputInt text = do
    putStrLn text
    input <- getLine
    if checkValidInt input
        then return (read input :: Int)
        else do
            putStrLn "Valor incorrecto !!"
            getValidaInputInt text

getValidaInputIntWithValidationRange :: String -> [Int] -> IO Int
getValidaInputIntWithValidationRange text range = do
    putStrLn text
    input <- getLine
    if checkValidInt input && ((read input :: Int) `elem` range)
        then return (read input :: Int)
        else do
            putStrLn "Valor incorrecto !!"
            getValidaInputIntWithValidationRange text range

checkValidInt :: String -> Bool
checkValidInt strInt = Nothing /= (readMaybe strInt :: Maybe Int)