## Trabajo Final de Haskell

**Autor: Rodrigo Souza**

Para el trabajo final se me pido que hiciera un juego en Haskell, y decidi hacer el juego de **poker**. Lo primero que hice fue investigar el juego en si; luego me puse a pensar en formas de implementarlo
en haskell, esto lo hice investigando en internet viendo libreririas y ejemplos de otros juegos de cartas. Una vez que tuve lo necesario para empezar a desarrollar me surgio la duda de como manejar
los modulos y la estructura del proyecto, para ello encontre la solucion en una herramienta llamada [Stack](https://docs.haskellstack.org/en/stable/GUIDE/). La herramienta **stack** te ayuda a crear
una estructura de proyecto basica con la cual comenzar, tambien brinda la posibilidad de agregar modulos al proyecto de una manera sencilla y mucho mas. Cuando finalmente comence a desarrolar empece 
por crear un modulo que se encargaria de la  representacion de las cartas, el metodo que genera el mazo como tambien el que mezcla el mazo. Luego empece a desarrollar la interaccion del juego con el 
ser humano, la asignacion de cartas, las que el usuario desea descartar para obtener nuevas.

Luego de tener resuelto la interaccion con el jugador, comence por desarrollar la forma en que pudiera detectar las manos posbiles(jugadas posibles). La deteccion de las posibles jugadas lo resolvi
agrupando por numero iguales de las cartas de esta manera es facil detectar si hay pares, full house, etc; si bien puedo detectar la manos relacionadas con los numeros, debio a falta de tiempo no puede
realizar la deteccion de otras manos un poco mas complicadas como escalera, escalera real, color, etc; donde se entran en juego el numero y tipo de carta. 
Por ultimo  para hacer el juego un poco mas interesante agregue un npc(Personaje no jugable) que basicamente se le asigna una mano como al jugador y el sistema detecta si tien algo o no, luego de esto
se compara la mano del jugador con la del npc y el que tenga la mano mas alta gana.

Si bien el juego no parece tan complejo me llevo mucho tiempo entender bien la nomenclatura de **Haskell** y le dedique mucho tiempo a en entender los tipos de clases como por ejempplo los tipo IO.
Graciasa la herramienta **Stack** pude compilar el  proyecto y ejecutarlo de una manera mas facil como asi tambien poder detectar los errores.

## Pasos para compilar/ejecutar/probar/jugar

Los siguiente pasos son para compilar y ejecutar el proyecto.

1. Descargar e instalar Stack([Paso a paso](https://docs.haskellstack.org/en/stable/install_and_upgrade/))
2. Navegar a la carpeta "poker-game"
3. Ejecutar el comando **stack build** (Compila el proyecto y descarga todos los modulos necesarios).
4. Ejecutar el comando **stack exec poker-game-exe**


Los siguientes pasos son para jugar el juego.

5. **Seleccionar cuantas cartas que quieres devolver/Cambiar**: Al comenzar el juego, se te asiganara 5 cartas y te mostrara tus cartas. Luego te preguntara cuantas quieres devolver, si seleccionas 0
    no devolvera nada y seguiras con la mano original, por otro lado si seleccionas una o mas te dira cuales quieres eliminar.
6. **Cartas a devolver**: Se temostrara un rango que hace referencia al indice donde se encuentra la carta en la lista. Te preguntara tantas veces en base a las cartas que pediste devolver. Luego
    tomara cartas del mazo para volver a completar las 5 cartas de tu mano.
7. **Volver a jugar?**: Una vez hallas elegidos tus cartas el sistema mostrara tu nueva mano, la mano del npc y si tienen alguna mano ganadora, luego el sistema comparara las manos para determinar
    quien gano. Por ultimo el juego te preguntara si quieres volver a jugar, si apriestas **S** el juego volvera a empezar de lo contrario terminara.
## Bliografia en la que me base para hacer el proyecto

1. [Libro de introduccion a Haskell](http://aprendehaskell.es/main.html)
2. [Libreria de poker](https://www.stackage.org/haddock/lts-18.25/general-games-1.1.1/Game-Game-Poker.html)
3. [Libro de cartas estandar](https://www.stackage.org/haddock/lts-18.25/general-games-1.1.1/Game-Implement-Card-Standard.html#v:fullDeck)
4. [Stack(herramienta)](https://docs.haskellstack.org/en/stable/GUIDE/)